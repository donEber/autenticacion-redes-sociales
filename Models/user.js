// import mongoose from 'mongoose'
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    nombre: String,
    provider: String,
    provider_id: {type: String, unique:true},
    foto: String
});

module.exports = mongoose.model('User', UserSchema);