
var express = require('express'),
    favicon = require('serve-favicon'),
    jade = require('jade'),
    routes = require('./routes/index');
//URLS
var faviconURL = `${__dirname}/public/img/favicon.ico`,
    publicDir = express.static(`${__dirname}/public`),
    viewDir  = `${__dirname}/views`,
    port = (process.env.port || 3000),
    app = express();

const passport = require('passport');
const session = require('express-session')
require('./database')
require('./passport')
app
    //configurando la app
    .set('views', viewDir)
    .set('view engine', 'jade')
    .set('port', port)
    .use(express.urlencoded({ extended: false }))
    .use(session({
        secret: 'tuNoHasVistoNada',
        resave: true,
        saveUninitialized: true
    }))
    // Lo de passport
    .use(passport.initialize())
    .use(passport.session())
    .get('/auth/facebook',passport.authenticate('facebook'))
    .get('/auth/facebook/callback', passport.authenticate('facebook',{
        successRedirect: '/informacion',
        failureRedirect: '/'
    }))
    .get('/logout', (req,res)=>{
        req.logout();
        res.redirect('/');
    })
    //ejecutando middlewares
    .use( favicon(faviconURL) )
    .use(publicDir)
    .use('/', routes)

module.exports = app