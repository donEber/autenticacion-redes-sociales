const mongoose = require('mongoose')
const config = require('./config')
const facebookStrategy = require('passport-facebook').Strategy;
const passport = require('passport');
// const Usuario = mongoose.model('Usuario');
const Usuario = require('./Models/user');

passport.use(new facebookStrategy({
    clientID: '1074986176030782',
    clientSecret: '800c4e029e90f91e6822e5317d3e6ea7',
    callbackURL: '/auth/facebook/callback',
    profileFields: ['id', 'displayName', 'photos', 'email']
    // profileFields: ['id', 'displayName', 'provider']
}, function (accessToken, refreshToken, profile, done) {
    // Usuario.findOne({provider_id: profile.id}, async (err, user)=>{
    //     if(err) throw(err);
    //     if(!err && user!=null ) return done(null, user);
    //     const nuevoUsuario = new Usuario({
    //         nombre: profile.displayName ,
    //         provider: 'facebook' ,
    //         provider_id: profile.id,
    //         foto: profile.photos[0].value
    //     })
    //     await nuevoUsuario.save()
    //     done(null,nuevoUsuario)
    // })
    const nuevoUsuario = new Usuario({
        nombre: profile.displayName,
        provider: 'facebook',
        provider_id: profile.id,
        foto: profile.photos[0].value
    });
    // console.log(`Id: ${profile.id}, DisplayName: ${profile.displayName}, Photo: ${profile.photos[0].value}, Email: ${profile.email}`)
    console.log(`Id: ${nuevoUsuario.provider_id}, DisplayName: ${nuevoUsuario.nombre}, Photo: ${nuevoUsuario.foto}, Provider: ${nuevoUsuario.provider}`)
    return done(null, nuevoUsuario);
}))

passport.serializeUser((user, done) => {
    done(null, user)
})

passport.deserializeUser((obj, done) => {
    done(null, obj);
})