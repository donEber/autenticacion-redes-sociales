'use strict'
var express = require('express'),
    router = express.Router();
//funciones callbacks
function index(req, res, next){
    res.render('index')
}
function vistaInformacion(req, res, next) {
    let datos = {
        framework: 'Express Js',
        templateEngine: 'Jade',
        linkExpress: 'http://expressjs.com/en/guide/routing.html',
        linkJade: 'http://jade-lang.com/reference',
        user: req.user
    }
    res.render('informacion', datos)
}
function error404(req, res, next) {
    let error = new Error();
    let locals = {
        title: 'Error 404',
        descripcion: 'Recurso no encontrado',
        error
    }
    error.status = 404
    res.render('error404', locals)
    next()//ejecuta el sig middleware
}
//rutas
router
    .get('/', index)
    .get('/informacion', vistaInformacion)
    .use(error404)

module.exports = router
